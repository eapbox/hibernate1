package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.RoomHibernateDao;
import com.gmail.eapbox.repository.model.Room;

public class RoomHibernateDaoImpl
        extends GenericHibernateDaoImpl<Room, Integer>
        implements RoomHibernateDao{

        public RoomHibernateDaoImpl(Class<Room> persistentClass) {
                super(persistentClass);
        }
}
