package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.UserInformationHibernateDao;
import com.gmail.eapbox.repository.model.UserInformation;

public class UserInformationHibernateDaoImpl
        extends GenericHibernateDaoImpl<UserInformation, Integer>
        implements UserInformationHibernateDao{

        public UserInformationHibernateDaoImpl(Class<UserInformation> persistentClass) {
                super(persistentClass);
        }
}
