package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.Room;

public interface RoomHibernateDao extends GenericHibernateDao<Room, Integer>{
}
