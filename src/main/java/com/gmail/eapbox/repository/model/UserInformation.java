package com.gmail.eapbox.repository.model;

import java.io.Serializable;
import java.util.Objects;

public class UserInformation implements Serializable {
        private static final long serialVersionUID = 8029467976241746772L;

        private Integer id;
        private User user;
        private String address;

        public User getUser() {
                return user;
        }

        public void setUser(User user) {
                this.user = user;
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getAddress() {
                return address;
        }

        public void setAddress(String address) {
                this.address = address;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                UserInformation that = (UserInformation) o;
                return Objects.equals(id, that.id) &&
                        Objects.equals(address, that.address);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id, address);
        }

        @Override
        public String toString() {
                return "UserInformation(" +
                        "id=" + id +
                        "; address: " + address +")";
        }
}
