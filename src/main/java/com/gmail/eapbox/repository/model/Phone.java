package com.gmail.eapbox.repository.model;

import java.io.Serializable;
import java.util.Objects;

public class Phone implements Serializable {
        private static final long serialVersionUID = 7290228403560076516L;

        private Integer id;
        private Integer userId;
        private Integer phoneNumber;
        private User user;

        public User getUser() {
                return user;
        }

        public void setUser(User user) {
                this.user = user;
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getUserId() {
                return userId;
        }

        public void setUserId(Integer userId) {
                this.userId = userId;
        }

        public Integer getPhoneNumber() {
                return phoneNumber;
        }

        public void setPhoneNumber(Integer phoneNumber) {
                this.phoneNumber = phoneNumber;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Phone phone = (Phone) o;
                return Objects.equals(id, phone.id) &&
                        Objects.equals(userId, phone.userId) &&
                        Objects.equals(phoneNumber, phone.phoneNumber);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id, userId, phoneNumber);
        }

        @Override
        public String toString() {
                return "Phone(" +
                        "id=" + id +
                        "; userId=" + userId +
                        "; phoneNumber: " + phoneNumber +")";
        }
}
