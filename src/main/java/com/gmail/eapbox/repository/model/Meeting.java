package com.gmail.eapbox.repository.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Meeting implements Serializable{
        private static final long serialVersionUID = -7367358265418541094L;

        private Integer id;
        private String meetingName;
        private Room room;
        private Set<User> users = new HashSet<User>(0);

        public Set<User> getUsers() {
                return users;
        }

        public void setUsers(Set<User> users) {
                this.users = users;
        }

        public Room getRoom() {
                return room;
        }

        public void setRoom(Room room) {
                this.room = room;
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getMeetingName() {
                return meetingName;
        }

        public void setMeetingName(String meetingName) {
                this.meetingName = meetingName;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Meeting meeting = (Meeting) o;
                return Objects.equals(id, meeting.id) &&
                        Objects.equals(meetingName, meeting.meetingName);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id, meetingName);
        }

        @Override
        public String toString() {
                return "Meeting(" +
                        "id=" + id +
                        "; meetingName:" + meetingName +")";
        }
}
