package com.gmail.eapbox.repository.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class User implements Serializable {
        private static final long serialVersionUID = 1744184704090515776L;

        private Integer id;
        private String username;
        private UserInformation userInformation;
        private Set<Phone> phones = new HashSet<Phone>(0);
        private Set<Meeting> meetings = new HashSet<Meeting>(0);

        public Set<Meeting> getMeetings() {
                return meetings;
        }

        public void setMeetings(Set<Meeting> meetings) {
                this.meetings = meetings;
        }

        public Set<Phone> getPhones() {
                return phones;
        }

        public void setPhones(Set<Phone> phones) {
                this.phones = phones;
        }

        public UserInformation getUserInformation() {
                return userInformation;
        }

        public void setUserInformation(UserInformation userInformation) {
                this.userInformation = userInformation;
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                User user = (User) o;
                return Objects.equals(id, user.id) &&
                        Objects.equals(username, user.username);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id, username);
        }

        @Override
        public String toString() {
                return "User(" +
                        "id=" + id +
                        "; username" + username +")";
        }
}
