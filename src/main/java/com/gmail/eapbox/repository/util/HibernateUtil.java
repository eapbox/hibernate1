package com.gmail.eapbox.repository.util;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
        private static final Logger logger = Logger.getLogger(HibernateUtil.class);
        private static final SessionFactory sessionFactory = buildSessionFactory();

        private static SessionFactory buildSessionFactory() {
                try {
                        return new Configuration().configure().buildSessionFactory();
                        /*return new Configuration().configure().
                                setNamingStrategy(new CustomNamingStrategy())
                                .buildSessionFactory();
                        */

                } catch (Throwable ex) {
                        logger.error("cant configure sessionFactory");
                        throw new ExceptionInInitializerError(ex);
                }
        }

        public static SessionFactory getSessionFactory() {
                return sessionFactory;
        }

        public static Session getSession() {
                return sessionFactory.openSession();
        }
}
