package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.Meeting;

public interface MeetingHibernateDao extends GenericHibernateDao<Meeting, Integer>{
}
