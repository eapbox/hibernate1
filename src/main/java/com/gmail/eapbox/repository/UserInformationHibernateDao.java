package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.UserInformation;

public interface UserInformationHibernateDao extends GenericHibernateDao<UserInformation, Integer>{
}
