package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.exception.DaoHibernateException;

import java.util.List;

public interface GenericHibernateDao<T, ID> {
        ID insert(T entity) throws DaoHibernateException;
        void update(T entity) throws DaoHibernateException;
        void delete(ID i) throws DaoHibernateException;
        T findById(ID i) throws DaoHibernateException;
        List<T> findAll() throws DaoHibernateException;

}
