package com.gmail.eapbox;

import com.gmail.eapbox.repository.*;
import com.gmail.eapbox.repository.exception.DaoHibernateException;
import com.gmail.eapbox.repository.impl.*;
import com.gmail.eapbox.repository.model.*;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);

    private static final MeetingHibernateDao meetingHibernateDao = new MeetingHibernateDaoImpl(Meeting.class);
    private static final PhoneHibernateDao phoneHibernateDao = new PhoneHibernateDaoImpl(Phone.class);
    private static final RoomHibernateDao roomHibernateDao = new RoomHibernateDaoImpl(Room.class);
    private static final UserHibernateDao userHibernateDao = new UserHibernateDaoImpl(User.class);
    private static final UserInformationHibernateDao userInformationHibernateDao = new UserInformationHibernateDaoImpl(UserInformation.class);

    public static void main( String[] args ) throws DaoHibernateException {
        logger.info( "Hello World!" );

        User user = new User();
        Meeting meeting = new Meeting();
        Phone phone = new Phone();
        Room room = new Room();
        UserInformation userInformation = new UserInformation();

        //phone.setId(1);
        //phone.setPhoneNumber(111);
        //phone.setUserId(2);
        //Integer insert = phoneHibernateDao.insert(phone);

        //user = userHibernateDao.findById(2);
        //logger.info(user.getUsername());

        //userInformation.setId(5);
        //userInformation.setAddress("city5");
        //Integer insert = userInformationHibernateDao.insert(userInformation);

        //meeting.setMeetingName("first 5");
        //Integer insert = meetingHibernateDao.insert(meeting);
        //meetingHibernateDao.delete(2);
        //List<Meeting> meetings = meetingHibernateDao.findAll();
        //System.out.println(meetings);
        //System.out.println(meeting);

        //user.setId(11);
        //user.setUsername("User11");
        //Integer insert = userHibernateDao.insert(user);
        //user.setId(insert);
        //user.setUsername("User9_1");
        //userHibernateDao.update(user);
        //userHibernateDao.delete(3);

        /*
        //User - UserInformation (1-1) --------------------
        user.setUsername("User1");
        userInformation.setAddress("Street_6");
        user.setUserInformation(userInformation);
        userInformation.setUser(user);
        userHibernateDao.insert(user);
        //------------------------------------------------*/

        /*
        //Meeting - Room (1-1)------------------------------
        meeting.setMeetingName("meeting_22");
        room.setRoomName("room_22");
        room.setRoomNumber(22);

        meeting.setRoom(room);
        room.setMeeting(meeting);
        meetingHibernateDao.insert(meeting);
        //-----------------------------------------------*/

        /*
        //User - Phone (1-M)-------------------------------
        user.setUsername("User3");
        userHibernateDao.insert(user);

        phone.setPhoneNumber(3333);
        phone.setUser(user);
        user.getPhones().add(phone);
        phoneHibernateDao.insert(phone);
        //------------------------------------------------*/

        ///*
        //User - Meeting (M-M)-----------------------------
        user.setUsername("user1");

        Meeting meeting1 = new Meeting();
        Meeting meeting2 = new Meeting();
        meeting1.setMeetingName("meeting1");
        meeting2.setMeetingName("meeting2");

        Set<Meeting> meetings = new HashSet<Meeting>();
        meetings.add(meeting1);
        meetings.add(meeting2);

        user.setMeetings(meetings);
        userHibernateDao.insert(user);
        //------------------------------------------------*/


    }
}
